#!/bin/bash

. ~/.bash_profile

# Install rvm
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -sSL https://get.rvm.io | bash -s stable
if [ -a ~/.profile ]; then
  echo "source ~/.profile" >> .bash_profile
fi

source ~/.bash_profile
rvm get stable --autolibs=enable

# Install ruby
rvm install ruby-2.3.3


# Prepare Ruby for Rails
rvm gemset use global
echo "gem: --no-document" >> ~/.gemrc
gem update
gem install bundler
gem install nokogiri

rvm use ruby-2.3.3@gcrm --ruby-version --create

