#!/bin/bash

. ~/.bash_profile

cd /var/www/{{SITE_URL}} && bundle install
cd /var/www/{{SITE_URL}} && rake db:setup
