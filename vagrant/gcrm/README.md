This is the folder vagrant mounts as /vagrant.
The source code is linked from this folder. That means that you need to clone the git repository inside a gcrm-server/ folder here

The first time you launch the virtual machine it will be provisioned:

    $ vagrant up

installing:
 - NginX
 - PostgreSQL
 - ruby, rvm and rails
 - Puma
