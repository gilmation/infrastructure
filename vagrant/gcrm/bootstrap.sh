#!/bin/bash

id

grep -q "\<db\>" /etc/hosts
if [ $? -eq 0 ]; then
  echo "GILMATION BOOTSTRAP: Already provisioned"
  exit 0
fi


. /vagrant/gilmation_devops_tools.sh

# Repos for nginx
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
copyToVagrant /etc/yum.repos.d

yum update -y

# Install NodeJS
yumInstall nodejs

copyToVagrant /home/vagrant
#chown vagrant:vagrant /home/vagrant/install_*.sh
chmod +x /home/vagrant/install_*.sh

/home/vagrant/install_ruby.sh
#sudo -u vagrant /home/vagrant/install_ruby.sh

# Install DB and server
yumInstall postgresql-server postgresql postgresql-contrib libpqxx postgresql-devel nginx
yumInstall vim git

copyToVagrant /etc/nginx

echo "GILMATION BOOTSTRAP: updating hosts file ..."
addToHosts ""
if [ $DB_HOST == "127.0.0.1" ] || [ $DB_HOST == "localhost" ]; then
  local_db=1
  DB_HOST="127.0.0.1"
  addToHosts "$DB_HOST db localhost localhost.localdomain localhost4 localhost4.localdomain4"
else
  local_db=0
  addToHosts "$DB_HOST db"
fi 
cat /etc/hosts

echo "GILMATION BOOTSTRAP: generating SSL dev keys ..."
copyToVagrant /tmp
nginxFakeCerts $SITE_URL $CERT_FILE


echo "GILMATION BOOTSTRAP: copying config files..."

postgresql-setup initdb
copyToVagrant /var
service postgresql restart
systemctl enable postgresql

sudo -u postgres psql -c "CREATE USER gcrm CREATEDB SUPERUSER PASSWORD 'gcrm1234'"

mkdir -p /var/www
ln -s /vagrant/gcrm-server /var/www/$SITE_URL
#changePerm /var/www/$SITE_URL vagrant:nginx 775

/home/vagrant/install_rails_project.sh
#sudo -u vagrant /home/vagrant/install_rails_project.sh

rm -f /var/www/$SITE_URL/shared
mkdir -p /var/local/$SITE_URL/shared/{log,sockets,pids}
ln -s /var/local/$SITE_URL/shared /var/www/$SITE_URL/shared
chown -R nginx:nginx /var/local/$SITE_URL/shared 
chown -h vagrant:vagrant /var/www/$SITE_URL/shared

copyToVagrant /etc/systemd
changePerm /etc/systemd/system/puma.service root:root 660
systemctl daemon-reload

service nginx restart
systemctl enable nginx
service puma start
systemctl enable puma
