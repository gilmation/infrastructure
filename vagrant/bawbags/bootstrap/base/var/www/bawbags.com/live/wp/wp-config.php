<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '{{DB_BLOG_SCHEMA}}');

/** MySQL database username */
define('DB_USER', '{{DB_BLOG_USER}}');

/** MySQL database password */
define('DB_PASSWORD', '{{DB_BLOG_PWD}}');

/** MySQL hostname */
define('DB_HOST', 'db');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+)&k{};FO8V_DdrS{+6<,zlUD<iLCFEgvt|<+!jVz@MSnR%vGnU$+1]lIM{-&7gL');
define('SECURE_AUTH_KEY',  'c>7 .x;^l)b?&~[lh oc5F+36HIYIMVr:@%A7#oz]Mu`)vi- +wszn;VdB^gbgL=');
define('LOGGED_IN_KEY',    'xW+Yg>FFe$8-<PDY4?AoX2IWmqA4_yRI%v_Lx.;FkC/|)FyDMO]P_B}G!F.+(=^L');
define('NONCE_KEY',        't<kK3Y/t(}J+]hQKx_(U(?55gC5tI&7 )0ZXo)*WE4m~E1{d|!9.aG)_wV>pOg$z');
define('AUTH_SALT',        'K/62<}K$q6W$F$Z-/JQQ{+|vPw3vO#ol3{50Ov*Z#z01kP*z-kd{A[|cH,U|B|5I');
define('SECURE_AUTH_SALT', 'EMNU2PxqYi~ErSHg(Ft8CX5. d5!opEbsbP6$PMOuOX`pbAI-b^2$|q+[u^B%XLr');
define('LOGGED_IN_SALT',   'iIM&O?YA]W9$^t])yVz#CvpHhA^cz;zlY7%7@K~(jAL?*4m7xrI,|;RM|-3saVD%');
define('NONCE_SALT',       ':2 snB$C<@p.{QSTN3TEr%KFe1nm-Cif6G3~P}ufqs=#EHf#|U^j1M5JA4Yk:g@X');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'bb_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
//Disable File Edits
define('DISALLOW_FILE_EDIT', true);
