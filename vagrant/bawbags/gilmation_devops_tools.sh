#!/bin/bash

declare -a bootstrap_variables

# definitions file
function loadVariables() {
local i
local var
 if [ $# -ne 1 ]; then
   return
 fi

 . $1

 i=0
 echo "bootstrap.ini CONFIG:"
 for var in `cut -d "=" -f 1 $1 | grep -v "#"`; do
   bootstrap_variables[$i]=$var
   i=$[i+1]
   echo "  $var=${!var}"
 done
 echo
}

# $1: variable name
# $2: variable value
# $3: file
function replaceVariable() {
  sed 's:{{'$1'}}:'$2':g' $3
}

# $1: file
# $2: dest
function replaceVariables() {
local var
local tmp
local tmp2
local d

  if [ $# -ne 2 ]; then
    return
  fi

  tmp=`mktemp`
  cp -f $1 $tmp
  tmp2=`mktemp`

  for var in ${bootstrap_variables[*]}; do 
    if [ "x"${!var} == "x" ]; then
      continue
    fi

    replaceVariable $var ${!var} $tmp > $tmp2
    mv $tmp2 $tmp
  done

  d=`dirname $2`
  mkdir -p $d
  mv $tmp $2
}

# To be executed in the host machine ========================================
function generatePackageFiles() {
local f
local d
local file

  rm -rf bootstrap/pkg/*
  for file in `find bootstrap/base -type f`; do 
    f=`echo $file | sed 's:^bootstrap/base::g'`
    d=`dirname $f`
    mkdir -p bootstrap/pkg$d
    replaceVariables $file "bootstrap/pkg$f"
    echo "GILMATION BOOTSTRAP: generatePackageFiles() bootstrap/pkg$f generated"
  done

}

# To be executed in the VM ==================================================
# $*: package names
function yumInstall() {
local pkg

  for pkg in $*; do
    yum -q list installed $pkg >/dev/null 2>&1
    if [ $? -ne 0 ]; then
      echo "GILMATION BOOTSTRAP: yumInstall() installing $pkg ..."
      yum install $pkg -y
    else
      echo "GILMATION BOOTSTRAP: yumInstall() $pkg already installed"
    fi
  done
}

# $*: line contents
function addToHosts() {
  echo "GILMATION BOOTSTRAP: addToHosts() $*"
  echo $* >> /etc/hosts
}

# $1: Site Url
# $2: Cert file name
function nginxFakeCerts() {
local key_file
local url

  if [ $# -ne 2 ]; then
    echo "GILMATION BOOTSTRAP: nginxFakeCerts() not enough parameters [$1][$2]"
    return
  fi

  mkdir -p /etc/nginx/ssl
  key_file=`mktemp`
  url=$1
  openssl genrsa -des3 -passout pass:1234 -out $key_file 1024
  openssl rsa -in $key_file -passin pass:1234 -out /etc/nginx/ssl/$2.key
  chmod 600 /etc/nginx/ssl/$2.key
  echo "GILMATION BOOTSTRAP: nginxFakeCerts() $2.key generated"

  openssl req -new -config /tmp/ssl_config -passin pass:1234 -x509 -nodes -sha1 -days 365 -key $key_file -out /etc/nginx/ssl/$2.crt
  chmod 644 /etc/nginx/ssl/$2.crt
  echo "GILMATION BOOTSTRAP: nginxFakeCerts() $2.crt generated"

  rm -f $key_file
  rm -f /tmp/ssl_config
}

# $1: folder to copy
function copyToVagrant() {
local sub

  if [ $# -eq 0 ]; then
    sub=""
  else
    sub=$1
  fi
  for file in `find /vagrant/bootstrap/pkg$sub -type f`; do
    f=`echo $file | sed 's:^/vagrant/bootstrap/pkg::g'`
    d=`dirname $f`
    if [ ! -d $d ]; then
      mkdir -p $d
    fi
    cp -f $file $d
    echo "GILMATION BOOTSTRAP: copyToVagrant() $f copied"
  done
}

# $1: location
# $2: user:group
# $3: permissions
function changePerm() {
  echo "GILMATION BOOTSTRAP: changePerm() $1 - $2 - $3"
  chown -R $2 $1
  chmod -R $3 $1
}

# $1: mode <grant> or <create>
# $2: user
# $3: password
# $4: schema
function createMysqlDb() {
local pre

  if [ $# -ne 4 ]; then
    echo "GILMATION BOOTSTRAP: createmysqldb() missing parameters [$1][$2][$3][$4]"
    return
  fi

  pre=`mktemp`
  echo "create database \`$4\`;" > $pre
  if [ $1 == "create" ]; then
    echo "create user '$2' identified by '$3';" >> $pre
  fi
  echo "grant all on \`$4\`.* to '$2'@'localhost' identified by '$3';" >> $pre
  cat $pre
  mysql < $pre

  rm -f $pre
}

# $1: Dump file
function dumpMysqlDb() {
local sql

  if [ $# -lt 1 ]; then
    echo "GILMATION BOOTSTRAP: dumpMysqlDb() missing parameters $*"
    return
  fi

  if [ -f $1 ]; then
    sql=$1
  else
    sql="/vagrant/$1"
  fi
  if [ -f $sql ]; then
    if [ -f $sql.pre ]; then
      cat $sql.pre | mysql $2
      echo "GILMATION BOOTSTRAP: dumpMysqlDb($sql.pre) done"
    fi

    echo "GILMATION BOOTSTRAP: dumpMysqlDb($sql)..."
    cat $sql | mysql $2
    echo "GILMATION BOOTSTRAP: dumpMysqlDb($sql) done"

    if [ -f $sql.post ]; then
      cat $sql.post | mysql $2
      echo "GILMATION BOOTSTRAP: dumpMysqlDb($sql.post) done"
    fi
  else
    echo "GILMATION BOOTSTRAP: dumpMysqlDb() [$1] not found"
  fi
}

# $1: root folder var name (media, includes, var, wp)
# $2: link (media, includes, var, wp/wp-contents/uploads)
# $2: dest
# $3: permissions
# $4-: subfolders 
function magentoShared() {
local root
local dst
local perm
local curr_var
local current
local sub
local dest

  root=$1
  shift
  link=$1
  shift
  dst=$1
  shift
  perm=$1
  shift

  dest=$dst/$root

  curr_var=`echo $root | tr "[:lower:]" "[:upper:]"`
  curr_var="${curr_var}_FOLDER"

  if [ "x"${!curr_var} == "x" ]; then # It already exists in the code
    current=/var/www/$SITE_URL/live/$link
  else # It is external
    current="/vagrant/${!curr_var}"
  fi
 echo "GILMATION BOOTSTRAP: magentoShared() current $root is $current"
  mkdir -p $dest
  if [ -d $current ]; then
    echo "GILMATION BOOTSTRAP: magentoShared() copy $current to $dest"
    cp -r $current/* $dest/
  fi
  for sub in $*; do
    echo "GILMATION BOOTSTRAP: magentoShared() ensuring $root/$sub"
    mkdir -p $dest/$sub
  done

  changePerm $dest nginx:nginx $perm
  if [ -h /var/www/$SITE_URL/live/$link ]; then
    rm /var/www/$SITE_URL/live/$link
  else
    rm -rf /var/www/$SITE_URL/live/$link
  fi
  echo "GILMATION BOOTSTRAP: link $dest from $root"
  ln -s $dest /var/www/$SITE_URL/live/$link
}




if [ -a bootstrap.ini ]; then
  folder=""
else
  folder="/vagrant/"
fi

loadVariables ${folder}bootstrap.ini

if [ $# -eq 1 ] && [ $1 == "package" ]; then
  generatePackageFiles
elif [ $# -eq 1 ] && [ $1 == "clean" ]; then
  rm -rf bootstrap/pkg/*
fi
