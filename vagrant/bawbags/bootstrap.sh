#!/bin/bash

. /vagrant/gilmation_devops_tools.sh

grep -q "\<db\>" /etc/hosts
if [ $? -eq 0 ]; then
  echo "GILMATION BOOTSTRAP: Already provisioned"
  exit 0
fi

rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

copyToVagrant /etc/yum.repos.d

yum update -y

yumInstall git wget vim ImageMagick sendmail gcc kernel-headers kernel-devel
#yumInstall automake fuse-devel gcc-c++ git libcurl-devel libxml2-devel make openssl-devel fuse

yumInstall php56w php56w-opcache
yumInstall php56w-bcmath php56w-fpm php56w-gd php56w-mbstring php56w-mcrypt php56w-mysql php56w-soap php56w-xml php56w-intl

# Composer
curl https://getcomposer.org/installer > composer-setup.php
php composer-setup.php --install-dir=/usr/local/bin
rm -f composer-setup.php

# PHPUnit
wget https://phar.phpunit.de/phpunit.phar
chmod +x phpunit.phar
mv phpunit.phar /usr/local/bin/phpunit
/usr/local/bin/phpunit --version

# Mage Run
wget https://files.magerun.net/n98-magerun.phar
chmod +x n98-magerun.phar
mv n98-magerun.phar /usr/local/bin/


yumInstall nginx

wget http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm -O /vagrant/mysql-community-release-el7-5.noarch.rpm
rpm -ivh /vagrant/mysql-community-release-el7-5.noarch.rpm
yum update -y
yumInstall mysql-community-client
rm -f /vagrant/mysql-community-release-el7-5.noarch.rpm

echo "GILMATION BOOTSTRAP: updating hosts file ..."
addToHosts ""
if [ $DB_HOST == "127.0.0.1" ] || [ $DB_HOST == "localhost" ]; then
  local_db=1
  DB_HOST="127.0.0.1"
  addToHosts "$DB_HOST db localhost localhost.localdomain localhost4 localhost4.localdomain4"
else
  local_db=0
  addToHosts "$DB_HOST db"
fi 
cat /etc/hosts

echo "GILMATION BOOTSTRAP: generating SSL dev keys ..."
copyToVagrant /tmp

nginxFakeCerts $SITE_URL $CERT_FILE

echo "GILMATION BOOTSTRAP: copying config files..."
copyToVagrant /etc

echo "GILMATION BOOTSTRAP: fixing $SITE_URL install..."
mkdir /var/www/$SITE_URL
ln -s /vagrant/$SITE_FOLDER /var/www/$SITE_URL/live
copyToVagrant /var

if [ ! -f /var/www/$SITE_URL/live/index.php ]; then
  cp -f /var/www/$SITE_URL/live/index.php.live /var/www/$SITE_URL/live/index.php
fi

if [ ! -f /var/www/$SITE_URL/live/wp/wp-config.php ]; then
  cp -f /var/www/$SITE_URL/live/wp/wp-config.php.live /var/www/$SITE_URL/live/wp/wp-config.php
fi


echo "GILMATION BOOTSTRAP: media..."
magentoShared media media /var/local/$SITE_URL 775 js css catalog
echo "GILMATION BOOTSTRAP: wp uploads..."
magentoShared wp wp/wp-content/uploads /var/local/$SITE_URL 775 

echo "GILMATION BOOTSTRAP: var..."
magentoShared var var /var/local/$SITE_URL 775 cache log
changePerm /var/local/$SITE_URL/var/cache nginx:nginx 777

echo "GILMATION BOOTSTRAP: includes..."
magentoShared includes includes /var/local/$SITE_URL 775 src

chown -R vagrant:nginx /var/local/bawbags.com/media/*
chown -R vagrant:nginx /var/local/bawbags.com/includes/*
chown -R vagrant:nginx /var/local/bawbags.com/var/*

if [ $local_db -eq 1 ]; then
  yumInstall mysql-community-server
  service mysql start

  createMysqlDb "create" $DB_USER $DB_PWD $DB_SCHEMA
  dumpMysqlDb $DB_DATA

  createMysqlDb "create" $DB_BLOG_USER $DB_BLOG_PWD $DB_BLOG_SCHEMA
  dumpMysqlDb $DB_BLOG_DATA
fi

echo "GILMATION BOOTSTRAP: Starting services..." 
service php-fpm restart
service nginx restart

systemctl enable php-fpm
systemctl enable nginx
