The Vagrant provisioner script is bootstrap.sh. 

bootstrap.sh uses +gilmation\_devops\_tools.sh+ to install all required packages and copy a set of config files to the VM. 

These copied files must be stored at bootstrap/base. The folder structure is used to determine where the files have to be
copied. Any of these files may contain variable information, such as +{{SITE\_URL}}+.

Before the provisioning is executed in the VM, we need to `compile` these files so that the variable information gets
substituted by actual values defined typically in +bootstrap.ini+:

 +./gilmation\_devops\_tools.sh package+

This command will generate the same folder structure in bootstrap/pkg. 
substituted by actual values according to bootstrap.ini.

Once this is done we can execute +vagrant up+.


If +DB\_HOST+ is other than localhost, we we need two databases (store and blog) in the host machine:

+create database `bawbags\_store`;+
+create user 'bawbags\_store' identified by 'bar88888';+
+grant all on `bawbags\_store\_uk`.\* to 'bawbags\_store';+

The host machine IP must be passed to the scripts definingn +DB\_HOST+ in bootstrap.ini. This is an example of an ini file chunk:

+DB\_HOST=192.168.72.1+
+SITE\_URL=bawbags.com+
+CERT\_FILE=bawbags.com+
+CERT\_COUNTRY=GB+
+SITE\_FOLDER=bawbags\_magento/web+
+MEDIA\_FOLDER=media+

If the host is set to localhost then this is done automatically in the virtual server

+SITE\_FOLDER+ must contain a copy of the site source and it is accessd on the host machine:

+git clone git@bitbucket.org:bawbags/bawbags\_magento.git+

+MEDIA\_FOLDER+ must contain Magento's media folder. This folder has to be stored at the same level than Vagrantfile and is copied
into the virtual machine.

====
For the impatient:

mkdir bawbags-vagrant
cd bawbags-vagrant
git clone git@bitbucket.org:bawbags/bawbags\_magento.git
copy the media folder and the wp-content/uploads folder
Edit bootstrap.ini accordingly
./gilmation\_devops\_tools.sh package
vagrant up
Get the VM IP and add it to the host /etc/hosts:
IP   bawbags.com www.bawbags.com admin.bawbags.com

===
Fix the kernel error:

`$ sudo su -`
$ vmware-config-tools.pl`

 
