#!/bin/bash

function check_group() {
local t
  if [ $# -ne 1 ]; then
    echo "Usage $(basename $0) checks_group"
    exit -1
  fi

  t=`mktemp`
  for check in `find $checks_folder/$1 -type f`; do
    execute_check $check $1
  done > $t
  
  . $config_folder/default.cfg
  sendAlerts $t   
  rm -f $t
}

function execute_check() {
local check
local group
local instance_cfg
local t
local alert
local base_check

  check=`basename $1`
  group=$2

  mkdir -p $config_folder/$group
  for instance_cfg in `find $config_folder/$group -name "${check}-*.cfg"`; do 
    base_check=`basename $instance_cfg .cfg`
    reloadVariables $group $instance_cfg
    unset doRun
    
    . $checks_folder/$group/$check

    t=`mktemp`
    doRun > $t
    if [ $RETURN_VALUE -lt $MIN_ALLOWED ] || [ $RETURN_VALUE -gt $MAX_ALLOWED ]; then
      alert=1
    else
      alert=0
    fi

    mkdir -p $logs_folder/$group
    IFS="
"
    doLog $alert $t $group $base_check
    alert=$?
    processAlert $alert

    rm -f $t

    unsetVariables $group $instance_cfg
  done
}

function processAlert() {
local mod
  if [ $alert -gt 0 ]; then
    if [ "x"$SUPRESS_LOG == "x" ]; then
      SUPRESS_LOG=1
    fi
    mod=$(($alert % $SUPRESS_LOG))
    if [ $alert -eq 1 ] || [ $mod -eq 0 ]; then
      alertBody
    fi
  fi
}

function sendAlerts() {
local machine
  diff $1 /dev/null > /dev/null
  if [ $? -ne 0 ]; then
    machine=`hostname`
    if [ "x"$MAIL_TO != "x" ]; then
      if [ "x"$MAIL_CC == "x" ]; then
        cat $1 | mail -s "[$machine] Server alerts" $MAIL_TO
      else
        cat $1 | mail -s "[$machine] Server alerts" -c $MAIL_CC $MAIL_TO
      fi
    fi
  fi
}

function doLog() {
local alert
local prev
local line

  if [ $# -ne 4 ]; then
    return
  fi

  mkdir -p $logs_folder/$3
  touch $logs_folder/$3/$4.log
  if [ $1 -eq 0 ]; then
    alert=0
  else
    prev=`tail -n 1 $logs_folder/$3/$4.log | cut -d "," -f 2`
    if [ "x"$prev == "x" ]; then
      prev=0
    fi
    alert=$[prev+1]
  fi

  for line in `cat $2`; do
    echo "$DATE_TIME,$alert,$line"
  done >> $logs_folder/$3/$4.log

  return $alert
}

function reloadVariables() {
  if [ -f $config_folder/$1/default.cfg ]; then
    . $config_folder/$1/default.cfg
  fi

  if [ -f $config_folder/default.cfg ]; then
    . $config_folder/default.cfg
  fi

  if [ -f $2 ]; then
    . $2
  fi
}

function unsetVariables() {
  for var in `cat $config_folder/$1/default.cfg $config_folder/default.cfg $2 2> /dev/null | sort -u | awk 'BEGIN{FS="="}{print $1}'`; do
    unset $var
  done
  unset RETURN_VALUE
}

DATE=`date +%Y-%m-%d`
TIME=`date +%H:%M:%S`
DATE_TIME="${DATE} ${TIME}"

folder=`dirname $0`

checks_folder=$folder/checks
config_folder=$folder/config
logs_folder=$folder/logs

check_group $1

