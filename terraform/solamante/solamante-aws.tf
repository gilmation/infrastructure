provider "aws" {
  region = "eu-west-1"
}

# Create the VPC
resource "aws_vpc" "solamante-vpc" {
  cidr_block = "10.0.0.0/16"
  #instance_tenancy = "dedicated" # Shared by default
  tags {
    Name = "Solamante VPC"
  }
}

# Create an internet gateway to give our subnet access to the outside world
resource "aws_internet_gateway" "solamante-igw" {
  vpc_id = "${aws_vpc.solamante-vpc.id}"
}



# Create route tables
resource "aws_route_table" "solamante-web-route-table" {
  vpc_id = "${aws_vpc.solamante-vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.solamante-igw.id}"
  }
  # The local route (10.0.0.0/16) is created be default
  tags {
    Name = "solamante-web-route-table"
  }
}

# Create a subnet to launch our web instances into
resource "aws_subnet" "solamante-web-subnet" {
  vpc_id = "${aws_vpc.solamante-vpc.id}"
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone = "eu-west-1a"
}

# Associate the web route table to the web subnet
resource "aws_route_table_association" "solamante-web-subnet-assoc" {
  subnet_id = "${aws_subnet.solamante-web-subnet.id}"
  route_table_id = "${aws_route_table.solamante-web-route-table.id}"
}

# Create a subnet to launch our db instances into
resource "aws_subnet" "solamante-db-subnet" {
  vpc_id = "${aws_vpc.solamante-vpc.id}"
  cidr_block = "10.0.2.0/24"
  availability_zone = "eu-west-1b"
}

# Create a subnet to launch our db instances into
resource "aws_subnet" "solamante-db-subnet-1" {
  vpc_id = "${aws_vpc.solamante-vpc.id}"
  cidr_block = "10.0.3.0/24"
  availability_zone = "eu-west-1c"
}

# Our default security group to access
# the instances over SSH, HTTP and HTTPS. Let all traffic out.
resource "aws_security_group" "solamante-default-sg" {
  name = "solamante-default-sg"
  description = "Used by VPC"
  vpc_id = "${aws_vpc.solamante-vpc.id}"

  # SSH access from anywhere
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  # HTTP access from anywhere
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTPS access from anywhere
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create the VPC security groups - one for apps and one for the db
# Our default security group to access
# the instances over SSH, HTTP and HTTPS
resource "aws_security_group" "solamante-web-sg" {
  name = "solamante-web-sg"
  description = "For the solamante web servers"
  vpc_id = "${aws_vpc.solamante-vpc.id}"

  # SSH access from anywhere
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from anywhere
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTPS access from anywhere
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# DB Security Group
resource "aws_security_group" "solamante-db-sg" {
  name = "solamante-db-sg"
  description = "For the solamante db servers"
  vpc_id = "${aws_vpc.solamante-vpc.id}"


  # DB Access from the web
  ingress {
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    security_groups = ["${aws_security_group.solamante-web-sg.id}"]
  }
}

# Create an app server from the app AMI and add it to the app security group
resource "aws_instance" "solamante-web" {
  ami = "ami-b0ac25c3"
  instance_type = "t2.small"
  associate_public_ip_address = true
  subnet_id = "${aws_subnet.solamante-web-subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.solamante-web-sg.id}"]
  key_name = "hugh"
  private_ip = "10.0.1.133"
  tags {
    Name = "solamante-web"
  }

  # Connect the app server to the DB
  # We run a remote provisioner on the instance after creating it.
  # In this case, we just install nginx and start it. By default,
  # this should be on port 80
//  provisioner "remote-exec" {
//    inline = [
//      "sudo apt-get -y update",
//      "sudo apt-get -y install nginx",
//      "sudo service nginx start"
//    ]
//  }
}

resource "aws_eip" "solamante-eip" {
  vpc = true
  instance = "${aws_instance.solamante-web.id}"
  associate_with_private_ip = "10.0.1.133"
}

# Create an db server and add it to the db security group
resource "aws_db_parameter_group" "solamante-rds-parameters" {
  name = "solamante-rds-parameters"
  family = "mysql5.6"
  description = "RDS Solamante parameter group"

  parameter {
    name = "character_set_server"
    value = "utf8"
  }

  parameter {
    name = "character_set_client"
    value = "utf8"
  }
}

resource "aws_db_subnet_group" "solamante-db-subnet-group" {
  name = "solamante-db-subnet-group"
  description = "Solamante DB subnets"
  subnet_ids = ["${aws_subnet.solamante-db-subnet.id}","${aws_subnet.solamante-db-subnet-1.id}"]
  tags {
    Name = "Solamante DB subnet group"
  }
}

resource "aws_db_instance" "solamante-db" {
  allocated_storage    = 20
  engine               = "mysql"
  engine_version       = "5.6.27"
  instance_class       = "db.t2.micro"
  name                 = "solamante_store_uk"
  username             = "solamante_store"
  password             = "bar88888"
  db_subnet_group_name = "${aws_db_subnet_group.solamante-db-subnet-group.name}"
  parameter_group_name = "${aws_db_parameter_group.solamante-rds-parameters.name}"
  vpc_security_group_ids = ["${aws_security_group.solamante-db-sg.id}"]
  multi_az = false
}

output "DB_ADDRESS" {
  value = "${aws_db_instance.solamante-db.address}"
}

output "ELASTIC_IP" {
  value = "${aws_eip.solamante-eip.public_ip}"
}