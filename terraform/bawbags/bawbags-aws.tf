provider "aws" {
  region = "eu-west-1"
  profile = "bawbags"
}

# Create the VPC
resource "aws_vpc" "bawbags-vpc" {
  cidr_block = "10.0.0.0/16"
  #instance_tenancy = "dedicated" # Shared by default
  tags {
    Name = "Bawbags VPC"
  }
}

# Create an internet gateway to give our public subnet access to the outside world
resource "aws_internet_gateway" "bawbags-igw" {
  vpc_id = "${aws_vpc.bawbags-vpc.id}"
}

# Create route tables
resource "aws_route_table" "bawbags-dmz-route-table" {
  vpc_id = "${aws_vpc.bawbags-vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.bawbags-igw.id}"
  }
  # The local route (10.0.0.0/16) is created be default
  tags {
    Name = "bawbags-web-route-table"
  }
}

# Create a public subnet to launch our elb and admin instances into
resource "aws_subnet" "bawbags-dmz-subnet" {
  vpc_id = "${aws_vpc.bawbags-vpc.id}"
  cidr_block = "10.0.0.0/24"
  availability_zone = "eu-west-1a"
}

# Associate the web route table to the dmz subnet
resource "aws_route_table_association" "bawbags-dmz-subnet-assoc" {
  subnet_id = "${aws_subnet.bawbags-dmz-subnet.id}"
  route_table_id = "${aws_route_table.bawbags-dmz-route-table.id}"
}

# Create a private subnet to launch our web instances into
resource "aws_subnet" "bawbags-web-subnet" {
  vpc_id = "${aws_vpc.bawbags-vpc.id}"
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone = "eu-west-1a"
}

# Associate the web route table to the web subnet
resource "aws_route_table_association" "bawbags-web-subnet-assoc" {
  subnet_id = "${aws_subnet.bawbags-web-subnet.id}"
  route_table_id = "${aws_route_table.bawbags-dmz-route-table.id}"
}

# Create a subnet to launch our db instances into
# We must have two availability zones
resource "aws_subnet" "bawbags-db-subnet" {
  vpc_id = "${aws_vpc.bawbags-vpc.id}"
  cidr_block = "10.0.2.0/24"
  availability_zone = "eu-west-1b"
}

# Create a subnet to launch our db instances into
resource "aws_subnet" "bawbags-db-subnet-1" {
  vpc_id = "${aws_vpc.bawbags-vpc.id}"
  cidr_block = "10.0.3.0/24"
  availability_zone = "eu-west-1c"
}

# Our default security group to access
# the instances over SSH, HTTP and HTTPS. Let all traffic out.
resource "aws_security_group" "bawbags-default-sg" {
  name = "bawbags-default-sg"
  description = "Used by VPC"
  vpc_id = "${aws_vpc.bawbags-vpc.id}"

  # SSH access from anywhere
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  # HTTP access from anywhere
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTPS access from anywhere
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create the VPC security groups - admin, elb, web and db
# Our default security group to access
# the instances over SSH, HTTP and HTTPS
resource "aws_security_group" "bawbags-admin-sg" {
  name = "bawbags-admin-sg"
  description = "For the bawbags admin servers"
  vpc_id = "${aws_vpc.bawbags-vpc.id}"

  # SSH access from anywhere
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTPS access from anywhere
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "bawbags-elb-sg" {
  name = "bawbags-elb-sg"
  description = "For the bawbags elb servers"
  vpc_id = "${aws_vpc.bawbags-vpc.id}"

  # HTTP access from anywhere
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTPS access from anywhere
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "bawbags-web-sg" {
  name = "bawbags-web-sg"
  description = "For the bawbags web servers"
  vpc_id = "${aws_vpc.bawbags-vpc.id}"

  # SSH access from the admin group
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    security_groups = ["${aws_security_group.bawbags-admin-sg.id}"]
  }

  # HTTP access from anywhere
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    security_groups = ["${aws_security_group.bawbags-elb-sg.id}"]
  }

  # outbound internet access
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# DB Security Group
resource "aws_security_group" "bawbags-db-sg" {
  name = "bawbags-db-sg"
  description = "For the bawbags db servers"
  vpc_id = "${aws_vpc.bawbags-vpc.id}"


  # DB Access from the admin and web group
  ingress {
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    security_groups = ["${aws_security_group.bawbags-web-sg.id}", "${aws_security_group.bawbags-admin-sg.id}"]
  }
}

# Create ELB
resource "aws_elb" "bawbags-elb" {
  name = "bawbags-elb"

  subnets         = ["${aws_subnet.bawbags-web-subnet.id}"]
  security_groups = ["${aws_security_group.bawbags-elb-sg.id}"]
  instances       = ["${aws_instance.bawbags-web.id}"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 443
    lb_protocol       = "https"
    ssl_certificate_id = "arn:aws:iam::256604119004:server-certificate/bawbags-com-2016/bawbags-com-2016"
  }
}

# Create an db server and add it to the db security group
resource "aws_db_parameter_group" "bawbags-rds-parameters" {
  name = "bawbags-rds-parameters"
  family = "mysql5.6"
  description = "RDS Bawbags parameter group"

  parameter {
    name = "character_set_server"
    value = "utf8"
  }

  parameter {
    name = "character_set_client"
    value = "utf8"
  }
}

resource "aws_db_subnet_group" "bawbags-db-subnet-group" {
  name = "bawbags-db-subnet-group"
  description = "Bawbags DB subnets"
  subnet_ids = ["${aws_subnet.bawbags-db-subnet.id}","${aws_subnet.bawbags-db-subnet-1.id}"]
  tags {
    Name = "Bawbags DB subnet group"
  }
}

resource "aws_db_instance" "bawbags-db" {
  depends_on = ["aws_db_subnet_group.bawbags-db-subnet-group","aws_security_group.bawbags-db-sg"]
  allocated_storage    = 20
  engine               = "mysql"
  engine_version       = "5.6.27"
  instance_class       = "db.t2.micro"
  identifier           = "bawbags-store-uk"
  name                 = "bawbags_store_uk"
  username             = "bawbags_store"
  password             = "bar88888"
  db_subnet_group_name = "${aws_db_subnet_group.bawbags-db-subnet-group.id}"
  parameter_group_name = "${aws_db_parameter_group.bawbags-rds-parameters.name}"
  vpc_security_group_ids = ["${aws_security_group.bawbags-db-sg.id}"]
  multi_az = true
}

resource "aws_db_instance" "bawbags-blog-db" {
  depends_on = ["aws_db_subnet_group.bawbags-db-subnet-group","aws_security_group.bawbags-db-sg"]
  allocated_storage    = 5
  engine               = "mysql"
  engine_version       = "5.6.27"
  instance_class       = "db.t2.micro"
  identifier           = "bawbags-blog-uk"
  name                 = "bawbags_blog_uk"
  username             = "bawbags_blog"
  password             = "bar99999"
  db_subnet_group_name = "${aws_db_subnet_group.bawbags-db-subnet-group.id}"
  parameter_group_name = "${aws_db_parameter_group.bawbags-rds-parameters.name}"
  vpc_security_group_ids = ["${aws_security_group.bawbags-db-sg.id}"]
  multi_az = true
}

# Create Admin Server
resource "aws_instance" "bawbags-admin" {
  ami = "ami-67631b14"
  instance_type = "t2.small"
  associate_public_ip_address = true
  subnet_id = "${aws_subnet.bawbags-dmz-subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.bawbags-admin-sg.id}"]
  key_name = "bawbags-aws"
  private_ip = "10.0.0.13"
  tags {
    Name = "bawbags-admin"
  }

#  provisioner "file" {
#    source = "./bawbags-admin.tar.gz"
#    destination = "/tmp"
#  }
#  provisioner "remote-exec" {
#    "inline" = [
#      "cd /tmp && tar zxvf bawbags-admin.tar.gz",
#      "sudo echo db ${aws_db_instance.bawbags-db.address} >> /etc/hosts"
#    ]
#  provisioner "remote-exec" {
#    "inline" = [ "cd /tmp && chmod +x bootstrap.sh && sudo ./bootstrap.sh admin" ]
#  }
}

# Associate the Elastic IP with the admin
resource "aws_eip" "bawbags-admin-eip" {
  vpc = true
  instance = "${aws_instance.bawbags-admin.id}"
  associate_with_private_ip = "10.0.0.13"
}


# Create an app server from the app AMI and add it to the web security group
resource "aws_instance" "bawbags-web" {
  ami = "ami-89671ffa"
  instance_type = "t2.small"
  subnet_id = "${aws_subnet.bawbags-web-subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.bawbags-web-sg.id}"]
  key_name = "bawbags-aws"
  private_ip = "10.0.1.133"
  tags {
    Name = "bawbags-web"
  }

#  provisioner "file" {
#    source = "./bawbags-web.tar.gz"
#    destination = "/tmp"
#  }
#  provisioner "remote-exec" {
#    "inline" = [
#      "cd /tmp && tar zxvf bawbags-web.tar.gz",
#      "sudo echo db ${aws_db_instance.bawbags-db.address} >> /etc/hosts"
#    ]
#  }
#  provisioner "remote-exec" {
#    "inline" = [ "cd /tmp && chmod +x bootstrap.sh && sudo ./bootstrap.sh web" ]
#  }
}

resource "aws_s3_bucket" "bawbags-media-s3" {
    bucket = "bawbags-media-s3"
    acl = "private"

    tags {
        Name = "Bawbags media Bucket"
        Environment = "Production"
    }
}

resource "aws_s3_bucket" "bawbags-wp-uploads-s3" {
    bucket = "bawbags-wp-uploads-s3"
    acl = "private"

    tags {
        Name = "Bawbags media Bucket"
        Environment = "Production"
    }
}

output "DB_ADDRESS" {
  value = "${aws_db_instance.bawbags-db.address}"
}
output "DB_BLOG_ADDRESS" {
  value = "${aws_db_instance.bawbags-blog-db.address}"
}

output "ADMIN_ELASTIC_IP" {
  value = "${aws_eip.bawbags-admin-eip.public_ip}"
}

output "ELB_DNS_NAME" {
  value = "${aws_elb.bawbags-elb.dns_name}"
}
