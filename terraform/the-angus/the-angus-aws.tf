provider "aws" {
  region = "eu-west-1"
  #credentials in $HOME/.aws/credentials
}

resource "aws_s3_bucket" "www-the-angus-bucket" {
  bucket = "www.the-angus.co.uk"
  acl = "public-read"
  website {
    redirect_all_requests_to = "https://the-angus.co.uk"
  }
}

resource "aws_s3_bucket" "the-angus-bucket" {
  bucket = "the-angus.co.uk"
  acl = "public-read"
  website {
    index_document = "index.html"
  }
}

resource "aws_cloudfront_distribution" "the-angus" {
  origin {
    domain_name = "the-angus.co.uk.s3-website-eu-west-1.amazonaws.com"
    origin_id   = "the-angus"

    custom_origin_config{
      http_port = 80
      https_port = 443
      origin_protocol_policy = "http-only"
      origin_ssl_protocols = ["TLSv1"]
    }
  }

  enabled             = true
  comment             = "The Angus"
  default_root_object = "index.html"

  logging_config {
    include_cookies = false
    bucket          = "gilmation-cloudfront-logs.s3.amazonaws.com"
    prefix          = "the-angus"
  }

  aliases = ["the-angus.co.uk"]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "the-angus"
    compress = true

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
      #locations        = ["US", "CA", "GB", "DE"]
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = false
    # This value is the ServerCertificateId which is returned from
    # aws iam list-server-certificates
    iam_certificate_id = "ASCAJ6M2OV24K5Q55CNOI"
    minimum_protocol_version = "TLSv1"
    ssl_support_method = "sni-only"
  }
}

resource "aws_route53_zone" "the-angus" {
  name = "the-angus.co.uk"
  comment = "the Angus Zone"
}

resource "aws_route53_record" "www" {
  zone_id = "${aws_route53_zone.the-angus.zone_id}"
  name = "www.the-angus.co.uk"
  type = "A"

    alias {
      name = "${aws_s3_bucket.www-the-angus-bucket.website_domain}"
      zone_id = "${aws_s3_bucket.www-the-angus-bucket.hosted_zone_id}"
      evaluate_target_health = false
    }

//  alias {
//    name = "${aws_cloudfront_distribution.the-angus.domain_name}"
//    zone_id = "${aws_cloudfront_distribution.the-angus.hosted_zone_id}"
//    evaluate_target_health = false
//  }
//  alias {
//    name = "${aws_route53_record.main.name}"
//    zone_id = "${aws_route53_record.main.zone_id}"
//    evaluate_target_health = false
//  }
}

resource "aws_route53_record" "main" {
  zone_id = "${aws_route53_zone.the-angus.zone_id}"
  name = "the-angus.co.uk"
  type = "A"

  alias {
    name = "${aws_cloudfront_distribution.the-angus.domain_name}"
    zone_id = "${aws_cloudfront_distribution.the-angus.hosted_zone_id}"
    evaluate_target_health = false
  }
}


resource "aws_route53_record" "google-site-ver" {
  zone_id = "${aws_route53_zone.the-angus.zone_id}"
  name = "the-angus.co.uk"
  type = "TXT"
  ttl = 86400
  records = ["google-site-verification=V68hQdk6JaTUD5OlFahuKNDhoCjzrlep7VpytHwbvfI"]
}