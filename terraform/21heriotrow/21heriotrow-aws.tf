provider "aws" {
  region = "eu-west-1"
  #credentials in $HOME/.aws/credentials
}

resource "aws_s3_bucket" "21heriotrow-bucket" {
  bucket = "21heriotrow.com"
  acl = "public-read"
  website {
    redirect_all_requests_to = "http://www.21heriotrow.com"
  }
}

resource "aws_s3_bucket" "www-21heriotrow-bucket" {
  bucket = "www.21heriotrow.com"
  acl = "public-read"
  website {
    index_document = "index.html"
  }
}

resource "aws_s3_bucket" "21heriotrow-gilmation-bucket" {
  bucket = "21heriotrow.gilmation.com"
  acl = "public-read"
  website {
    index_document = "index.html"
  }
}


resource "aws_cloudfront_distribution" "21heriotrow" {
  origin {
    domain_name = "21heriotrow.co.uk.s3-website-eu-west-1.amazonaws.com"
    origin_id   = "21heriotrow"

    custom_origin_config{
      http_port = 80
      https_port = 443
      origin_protocol_policy = "http-only"
      origin_ssl_protocols = ["TLSv1"]
    }
  }

  enabled             = true
  comment             = "No. 21 Heriot Row"
  default_root_object = "index.html"

  logging_config {
    include_cookies = false
    bucket          = "gilmation-cloudfront-logs.s3.amazonaws.com"
    prefix          = "21heriotrow"
  }

  aliases = ["www.21heriotrow.com"]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "21heriotrow"
    compress = true

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
      #locations        = ["US", "CA", "GB", "DE"]
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
    # This value is the ServerCertificateId which is returned from
    # aws iam list-server-certificates
    # iam_certificate_id = "ASCAJ6M2OV24K5Q55CNOI"
    # minimum_protocol_version = "TLSv1"
    # ssl_support_method = "sni-only"
  }
}

resource "aws_route53_zone" "21heriotrow" {
  name = "21heriotrow.com"
  comment = "No. 21 Heriot Row Zone"
}

resource "aws_route53_record" "www" {
  zone_id = "${aws_route53_zone.21heriotrow.zone_id}"
  name = "www.21heriotrow.com"
  type = "A"

  alias {
    name = "${aws_cloudfront_distribution.21heriotrow.domain_name}"
    zone_id = "${aws_cloudfront_distribution.21heriotrow.hosted_zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "main" {
  zone_id = "${aws_route53_zone.21heriotrow.zone_id}"
  name = "21heriotrow.com"
  type = "A"

  alias {
    name = "${aws_s3_bucket.21heriotrow-bucket.website_domain}"
    zone_id = "${aws_s3_bucket.21heriotrow-bucket.hosted_zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "mail" {
  zone_id = "${aws_route53_zone.21heriotrow.zone_id}"
  name = "mail.21heriotrow.com"
  type = "A"
  ttl = 3600
  records = ["79.170.44.58"]
}

resource "aws_route53_record" "mail-mx" {
  zone_id = "${aws_route53_zone.21heriotrow.zone_id}"
  name = "21heriotrow.com"
  type = "MX"
  ttl = 3517
  records = ["10 mail.21heriotrow.com"]
}

resource "aws_route53_record" "google-site-ver" {
  zone_id = "${aws_route53_zone.21heriotrow.zone_id}"
  name = "21heriotrow.com"
  type = "TXT"
  ttl = 86400
  records = ["google-site-verification=s3V-S-ocMKJZJorRwLHykrI69JJMtrYWODKTJK0gJDQ"]
}