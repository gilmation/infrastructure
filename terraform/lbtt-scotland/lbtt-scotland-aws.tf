provider "aws" {
  region = "eu-west-1"
  #credentials in $HOME/.aws/credentials
}

resource "aws_s3_bucket" "www-lbtt-scotland-bucket" {
  bucket = "www.lbtt-scotland.co.uk"
  acl = "public-read"
  website {
    redirect_all_requests_to = "https://lbtt-scotland.co.uk"
  }
}

resource "aws_s3_bucket" "lbtt-scotland-bucket" {
  bucket = "lbtt-scotland.co.uk"
  acl = "public-read"
  website {
    index_document = "index.html"
  }
}

resource "aws_cloudfront_distribution" "lbtt-scotland" {
  origin {
    domain_name = "lbtt-scotland.co.uk.s3.amazonaws.com"
    origin_id   = "lbtt-scotland"

    s3_origin_config {
      #origin_access_identity = "origin-access-identity/cloudfront/lbtt-scotland"
    }
  }

  enabled             = true
  comment             = "The LBTT Ember App"
  default_root_object = "index.html"

  logging_config {
    include_cookies = false
    bucket          = "gilmation-cloudfront-logs.s3.amazonaws.com"
    prefix          = "lbtt-scotland"
  }

  aliases = ["lbtt-scotland.co.uk"]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "lbtt-scotland"
    compress = true

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
      #locations        = ["US", "CA", "GB", "DE"]
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = false
    # This value is the ServerCertificateId which is returned from
    # aws iam list-server-certificates
    iam_certificate_id = "ASCAJGDINTCLWENLB4HFC"
    minimum_protocol_version = "TLSv1"
    ssl_support_method = "sni-only"
  }
}

resource "aws_route53_zone" "lbtt-scotland" {
  name = "lbtt-scotland.co.uk"
  comment = "The LBTT Scotland Zone"
}

resource "aws_route53_record" "www" {
  zone_id = "${aws_route53_zone.lbtt-scotland.zone_id}"
  name = "www.lbtt-scotland.co.uk"
  type = "A"

    alias {
      name = "${aws_s3_bucket.www-lbtt-scotland-bucket.website_domain}"
      zone_id = "${aws_s3_bucket.www-lbtt-scotland-bucket.hosted_zone_id}"
      evaluate_target_health = false
    }

//  alias {
//    name = "${aws_cloudfront_distribution.lbtt-scotland.domain_name}"
//    zone_id = "${aws_cloudfront_distribution.lbtt-scotland.hosted_zone_id}"
//    evaluate_target_health = false
//  }
//  alias {
//    name = "${aws_route53_record.main.name}"
//    zone_id = "${aws_route53_record.main.zone_id}"
//    evaluate_target_health = false
//  }
}

resource "aws_route53_record" "main" {
  zone_id = "${aws_route53_zone.lbtt-scotland.zone_id}"
  name = "lbtt-scotland.co.uk"
  type = "A"

  alias {
    name = "${aws_cloudfront_distribution.lbtt-scotland.domain_name}"
    zone_id = "${aws_cloudfront_distribution.lbtt-scotland.hosted_zone_id}"
    evaluate_target_health = false
  }
}


resource "aws_route53_record" "google-site-ver" {
  zone_id = "${aws_route53_zone.lbtt-scotland.zone_id}"
  name = "lbtt-scotland.co.uk"
  type = "TXT"
  ttl = 86400
  records = ["google-site-verification=2_gpQBpfmNzxrT1E9wT6uw2uGRcIcuYa7QOAJ0luhSA"]
}