provider "aws" {
  region = "eu-west-1"
  #credentials in $HOME/.aws/credentials
}

resource "aws_s3_bucket" "www-confidas-people-bucket" {
  bucket = "www.confidas-people.co.uk"
  acl = "public-read"
  website {
    redirect_all_requests_to = "https://confidas-people.co.uk"
  }
}

resource "aws_s3_bucket" "confidas-people-bucket" {
  bucket = "confidas-people.co.uk"
  acl = "public-read"
  website {
    index_document = "index.html"
  }
}

resource "aws_s3_bucket" "confidas-people-gilmation-bucket" {
  bucket = "confidas-people.gilmation.com"
  acl = "public-read"
  website {
    index_document = "index.html"
  }
}

resource "aws_cloudfront_distribution" "confidas-people" {
  origin {
    domain_name = "confidas-people.co.uk.s3-website-eu-west-1.amazonaws.com"
    origin_id   = "confidas-people"

    custom_origin_config{
      http_port = 80
      https_port = 443
      origin_protocol_policy = "http-only"
      origin_ssl_protocols = ["TLSv1"]
    }
  }

  enabled             = true
  comment             = "Confidas People"
  default_root_object = "index.html"

  logging_config {
    include_cookies = false
    bucket          = "gilmation-cloudfront-logs.s3.amazonaws.com"
    prefix          = "confidas-people"
  }

  aliases = ["confidas-people.co.uk"]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "confidas-people"
    compress = true

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
      #locations        = ["US", "CA", "GB", "DE"]
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
    # This value is the ServerCertificateId which is returned from
    # aws iam list-server-certificates
    # iam_certificate_id = "ASCAJ6M2OV24K5Q55CNOI"
    # minimum_protocol_version = "TLSv1"
    # ssl_support_method = "sni-only"
  }
}

resource "aws_route53_zone" "confidas-people" {
  name = "confidas-people.co.uk"
  comment = "Confidas People Zone"
}

resource "aws_route53_record" "www" {
  zone_id = "${aws_route53_zone.confidas-people.zone_id}"
  name = "www.confidas-people.co.uk"
  type = "A"

  alias {
    name = "${aws_s3_bucket.www-confidas-people-bucket.website_domain}"
    zone_id = "${aws_s3_bucket.www-confidas-people-bucket.hosted_zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "main" {
  zone_id = "${aws_route53_zone.confidas-people.zone_id}"
  name = "confidas-people.co.uk"
  type = "A"

  alias {
    name = "${aws_cloudfront_distribution.confidas-people.domain_name}"
    zone_id = "${aws_cloudfront_distribution.confidas-people.hosted_zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "mail-mx" {
  zone_id = "${aws_route53_zone.confidas-people.zone_id}"
  name = "confidas-people.co.uk"
  type = "MX"
  ttl = 10800
  records = ["10 spool.mail.gandi.net.", "50 fb.mail.gandi.net."]
}

resource "aws_route53_record" "mail-spf" {
  zone_id = "${aws_route53_zone.confidas-people.zone_id}"
  name = "confidas-people.co.uk"
  type = "TXT"
  ttl = 86400
  records = ["v=spf1 include:_mailcust.gandi.net ?all"]
}

//resource "aws_route53_record" "assets-cert" {
//  zone_id = "${aws_route53_zone.confidas-people.zone_id}"
//  name = "confidas-people.co.uk"
//  type = "CNAME"
//  ttl = 10800
//  records = [""]
//}

//resource "aws_route53_record" "google-site-ver" {
//  zone_id = "${aws_route53_zone.confidas-people.zone_id}"
//  name = "confidas-people.co.uk"
//  type = "TXT"
//  ttl = 86400
//  records = ["google-site-verification=s3V-S-ocMKJZJorRwLHykrI69JJMtrYWODKTJK0gJDQ"]
//}