provider "aws" {
  region = "eu-west-1"
  #credentials in $HOME/.aws/credentials
}

resource "aws_s3_bucket" "app-aimee-assistant-bucket" {
  bucket = "app.aimee-assistant.com"
  acl = "public-read"
  website {
    index_document = "index.html"
  }
}

resource "aws_cloudfront_distribution" "app-aimee-assistant" {
  origin {
    domain_name = "app.aimee-assistant.com.s3-website-eu-west-1.amazonaws.com"
    origin_id   = "app-aimee-assistant"

    custom_origin_config{
      http_port = 80
      https_port = 443
      origin_protocol_policy = "http-only"
      origin_ssl_protocols = ["TLSv1"]
    }
  }

  enabled             = true
  comment             = "Aimee Assistant App"
  default_root_object = "index.html"

  logging_config {
    include_cookies = false
    bucket          = "gilmation-cloudfront-logs.s3.amazonaws.com"
    prefix          = "app-aimee-assistant"
  }

  aliases = ["app.aimee-assistant.com"]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "app-aimee-assistant"
    compress = true

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
      #locations        = ["US", "CA", "GB", "DE"]
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
    # Setup in AWS Console
  }
}