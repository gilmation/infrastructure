#!/bin/bash

declare -a bootstrap_variables

# definitions file
function loadVariables() {
local i
local var
 if [ $# -ne 1 ]; then
   return
 fi

 . $1

 i=0
 echo "$1 CONFIG:"
 for var in `cut -d "=" -f 1 $1 | grep -v "#"`; do
   bootstrap_variables[$i]=$var
   i=$[i+1]
   echo "  $var=${!var}"
 done
 echo
}

# $1: variable name
# $2: variable value
# $3: file
function replaceVariable() {
  sed 's:{{'$1'}}:'$2':g' $3
}

# $1: file
# $2: dest
function replaceVariables() {
local var
local tmp
local tmp2
local d

  if [ $# -ne 2 ]; then
    return
  fi

  tmp=`mktemp`
  cp -f $1 $tmp
  tmp2=`mktemp`

  for var in ${bootstrap_variables[*]}; do 
    if [ "x"${!var} == "x" ]; then
      continue
    fi

    replaceVariable $var ${!var} $tmp > $tmp2
    mv $tmp2 $tmp
  done

  d=`dirname $2`
  mkdir -p $d
  mv $tmp $2
}

# To be executed in the host machine ========================================
# $1: base folder
function generatePackageFiles() {
local f
local d
local file

  echo "GILMATION BOOTSTRAP: Generate package for $1"
  cleanPackageFiles
  for file in `find bootstrap/$1 -type f`; do 
    f=`echo $file | sed 's:^bootstrap/'$1'::g'`
    d=`dirname $f`
    mkdir -p bootstrap/pkg$d
    replaceVariables $file "bootstrap/pkg$f"
    echo "GILMATION BOOTSTRAP: $file generated in pkg"
  done
}

function cleanPackageFiles() {
  rm -rf bootstrap/pkg*
}

# To be executed in the VM ==================================================
# $*: package names
function yumInstall() {
local pkg

  for pkg in $*; do
    yum -q list installed $pkg >/dev/null 2>&1
    if [ $? -ne 0 ]; then
      echo "GILMATION BOOTSTRAP: installing $pkg ..."
      yum install $pkg -y
    else
      echo "GILMATION BOOTSTRAP: $pkg already installed"
    fi
  done
}

# $1: folder to copy
function copyToRemote() {
local sub

  if [ $# -eq 0 ]; then
    sub=""
  else
    sub=$1
  fi
  for file in `find /tmp/bootstrap/pkg$sub -type f`; do
    f=`echo $file | sed 's:^/tmp/bootstrap/pkg::g'`
    d=`dirname $f`
    cp $file $d
    echo "GILMATION BOOTSTRAP: $f copied"
  done
}

# $1: location
# $2: user:group
# $3: permissions
function changePerm() {
  echo "GILMATION BOOTSTRAP: $1 - $2 - $3"
  if [ $# -gt 1 ]; then
    chown -R $2 $1
    if [ $# -gt 2 ]; then
      chmod -R $3 $1
    fi
  fi
}


function installNginxCerts() {
local key_file
local url

  mkdir -p /etc/nginx/ssl
  if [ -a /etc/nginx/ssl/$CERT_FILE.key ] && [ -a /etc/nginx/ssl/$CERT_FILE.crt ]; then
    echo "GILMATION BOOTSTRAP: SSL keys already copied"
  else
    echo "GILMATION BOOTSTRAP: generating SSL dev keys ..."
    key_file=`mktemp`
    url=$SITE_URL
    echo "GILMATION BOOTSTRAP: openssl genrsa -des3 -passout pass:1234 -out $key_file 1024"
    openssl genrsa -des3 -passout pass:1234 -out $key_file 1024
    echo "GILMATION BOOTSTRAP: openssl rsa -in $key_file -passin pass:1234 -out /etc/nginx/ssl/$CERT_FILE.key"
    openssl rsa -in $key_file -passin pass:1234 -out /etc/nginx/ssl/$CERT_FILE.key
    openssl req -new -config /tmp/ssl_config -passin pass:1234 -x509 -nodes -sha1 -days 365 -key $key_file -out /etc/nginx/ssl/$CERT_FILE.crt
    echo "GILMATION BOOTSTRAP: openssl req -new -config /tmp/ssl_config -passin pass:1234 -x509 -nodes -sha1 -days 365 -key $key_file -out /etc/nginx/ssl/$CERT_FILE.crt"

    rm -f $key_file
    rm -f /tmp/ssl_config
  fi

  changePerm /etc/nginx/ssl/$CERT_FILE.key root:root 600
  changePerm /etc/nginx/ssl/$CERT_FILE.crt root:root 644
}

# $1: root folder (media, includes, var)
# $2: dest
# $3: permissions
# $4-: subfolders 
function magentoShared() {
local root
local dst
local perm
local curr_var
local current
local sub
local dest

  root=$1
  shift
  dst=$1
  shift
  perm=$1
  shift

  dest=$dst/$root

  curr_var=`echo $root | tr "[:lower:]" "[:upper:]"`
  curr_var="${curr_var}_FOLDER"

  if [ "x"${!curr_var} == "x" ]; then
    current=/var/www/$SITE_URL/bawbags_magento/web/$root
  else
    current="/tmp/${!curr_var}"
  fi
  echo "GILMATION BOOTSTRAP: current $root is $current"
  mkdir -p $dest
  if [ -d $current ]; then
    echo "GILMATION BOOTSTRAP: copy $current to $dest"
    cp -r $current/* $dest/
  fi
  for sub in $*; do
    echo "GILMATION BOOTSTRAP: ensuring $root/$sub"
    mkdir -p $dest/$sub
  done

  changePerm $dest nginx:nginx $perm
  if [ -h /var/www/$SITE_URL/bawbags_magento/web/$root ]; then
    rm -f /var/www/$SITE_URL/bawbags_magento/web/$root
  else
    rm -rf /var/www/$SITE_URL/bawbags_magento/web/$root
  fi
  echo "GILMATION BOOTSTRAP: link $dest from $root"
  ln -s $dest /var/www/$SITE_URL/bawbags_magento/web/$root
}

if [ $# -eq 0 ]; then
  cfg="bootstrap.ini"
elif [ -a bootstrap-$1.ini ]; then
  profile=$1
  cfg="bootstrap-$1.ini"
  shift
else
  profile="base"
  cfg="bootstrap.ini"
fi  

if [ -a $cfg ]; then
  folder=""
else
  folder="/tmp/"
fi

loadVariables ${folder}$cfg

if [ $# -eq 1 ]; then 
  if [ $1 == "package" ]; then
    generatePackageFiles $profile
  elif [ $1 == "clean" ]; then
    cleanPackageFiles $profile
  fi
fi
