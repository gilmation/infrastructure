#!/bin/bash

for i in web admin; do
  ./gilmation_devops_tools.sh $i package
  rm -f bawbags-$i.tar.gz
  tar zcvf bawbags-$i.tar.gz bootstrap* gilmation_devops_tools.sh local.xml media *.crt *.key
done
