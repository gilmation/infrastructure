#!/bin/bash

. /tmp/gilmation_devops_tools.sh $*

echo "GILMATION BOOTSTRAP: updating yum ..."
yum update -y

echo "GILMATION BOOTSTRAP: installing packages..."
yumInstall ImageMagick
yumInstall automake fuse-devel gcc-c++ git libcurl-devel libxml2-devel make openssl-devel fuse
yumInstall sendmail mailx
yumInstall php56
yumInstall php56-bcmath php56-fpm php56-gd php56-mbstring php56-mcrypt php56-mysqlnd php56-soap php56-intl php56-opcache
yumInstall nginx
yumInstall mysql

if [ "x"$DB_HOST != "x" ]; then
  echo "GILMATION BOOTSTRAP: updating hosts file ..."
  echo >> /etc/hosts
  echo "$DB_HOST db"
fi >> /etc/hosts

mkdir -p /run/php-fpm
echo "GILMATION BOOTSTRAP: copying config files..."
copyToRemote /etc

copyToRemote /tmp
installNginxCerts

echo "GILMATION BOOTSTRAP: Prepare ssh for git clone..."
copyToRemote /home
changePerm /home/ec2-user/.ssh/id_rsa ec2-user:ec2-user 600
changePerm /home/ec2-user/.ssh/id_rsa.pub ec2-user:ec2-user 644
runuser -l ec2-user -c "ssh-keyscan bitbucket.org >> /home/ec2-user/.ssh/known_hosts"
mkdir -p /var/www/$SITE_URL
changePerm /var/www/$SITE_URL ec2-user:ec2-user

echo "GILMATION BOOTSTRAP: git clone..."
runuser -l ec2-user -c "cd /var/www/$SITE_URL && git clone git@bitbucket.org:bawbags/bawbags_magento.git"


echo "GILMATION BOOTSTRAP: fixing $SITE_URL install..."
mkdir -p /var/www/$SITE_URL/bawbags_magento/web/app/etc
if [ -a /tmp/local.xml ]; then
  cp -f /tmp/local.xml /var/www/$SITE_URL/bawbags_magento/web/app/etc
fi
if [ ! -a /var/www/$SITE_URL/bawbags_magento/web/index.php ] && [ -a /var/www/$SITE_URL/bawbags_magento/web/index.php.live ]; then
  cp -f /var/www/$SITE_URL/bawbags_magento/web/index.php.live /var/www/$SITE_URL/bawbags_magento/web/index.php
fi

echo "GILMATION BOOTSTRAP: media..."
magentoShared media /var/local/$SITE_URL/bawbags_magento/web 775 js css catalog

echo "GILMATION BOOTSTRAP: var..."
magentoShared var /var/local/$SITE_URL/bawbags_magento/web 775 cache log tmp report
changePerm /var/local/$SITE_URL/bawbags_magento/web/var/cache deploy:nginx 777
changePerm /var/local/$SITE_URL/bawbags_magento/web/var/tmp deploy:nginx 770
changePerm /var/local/$SITE_URL/bawbags_magento/web/var/report deploy:nginx 770

echo "GILMATION BOOTSTRAP: includes..."
magentoShared includes /var/local/$SITE_URL/bawbags_magento/web 775 src

changePerm /var/www/$SITE_URL nginx:nginx


echo "GILMATION BOOTSTRAP: Starting services..." 
service php-fpm start
service nginx start

#systemctl enable php-fpm
#systemctl enable nginx
chkconfig nginx on
chkconfig php-fpm on
