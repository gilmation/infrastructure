### Install:

- Vagrant (Mac OSX Package)
- Terraform
- Packer

``` bash
/usr/local/hashicorp
$ ll
total 16
drwxrwxr-x  28 root  wheel   952 Mar  8 17:36 ..
drwxr-xr-x@ 37 root  wheel  1258 Mar  8 17:37 terraform_0.6.12_darwin_amd64
drwxr-xr-x   3 root  wheel   102 Mar  8 17:40 packer_0.9.0_darwin_amd64
lrwxr-xr-x   1 root  wheel    46 Mar  8 17:44 packer -> /usr/local/hashicorp/packer_0.9.0_darwin_amd64
lrwxr-xr-x   1 root  wheel    50 Mar  8 17:44 terraform -> /usr/local/hashicorp/terraform_0.6.12_darwin_amd64
drwxr-xr-x   6 root  wheel   204 Mar  8 17:44 .
```

### Add the ```/usr/local/hashicorp/packer``` and ```/user/local/hashicorp/terraform``` directories to your PATH

### Install the VMWare Plugin:

``` bash
$ vagrant plugin install vagrant-vmware-fusion
Installing the 'vagrant-vmware-fusion' plugin. This can take a few minutes...
Installed the plugin 'vagrant-vmware-fusion (4.0.8)'!
```
``` bash
$ vagrant plugin license vagrant-vmware-fusion /Dropbox/Gilmation_Dev/hashicorp/license.lic
Installing license for 'vagrant-vmware-fusion'...
The license for 'vagrant-vmware-fusion' was successfully installed!
```
``` bash
$ vagrant plugin list
vagrant-share (1.1.5, system)
vagrant-vmware-fusion (4.0.8)
```

### AWS Credentials ($HOME/.aws/credentials)

https://www.terraform.io/docs/providers/aws/

### 3 tier architecture information

http://blogs.aws.amazon.com/security/blog/tag/ELB

https://dzone.com/articles/aws-vpc-networking-beginners

http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Scenario2.html